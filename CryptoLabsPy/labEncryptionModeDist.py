import json
import requests
import base64
import os

from AES_SV import AES_SV as AES
from DATA import Data


        

def main():
    aes = AES()
    data = Data()

    message=os.urandom(aes.N)*100
    message=str(base64.b64encode(message))[2:-1]

    for i in range(data.ROUNDS):
        request = requests.post("http://"+data.IP_ADRESS+"/api/EncryptionModeOracle/" + data.ACCNAME + "/"+str(i), data=json.dumps(message), headers={'Content-Type': 'application/json', 'charset':'utf-8'})
        verify = requests.get("http://"+data.IP_ADRESS+"/api/EncryptionModeOracle/" + data.ACCNAME + "/"+str(i)+"/verify", headers={'Content-Type': 'application/json'})
        if verify.text==aes.ModeIS(request.text):
            print(verify.text + ' = ' + aes.ModeIS(request.text))
            print('[MODE]: ' + str(i) + ' TRUE')
        else:
            print('[MODE]: ' + str(i) + ' FALSE')
            
if __name__ == '__main__':
    main()