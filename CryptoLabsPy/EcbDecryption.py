import requests
import json
import base64

from AES_SV import AES_SV as AES
from DATA import Data


def main():
    
    aes = AES()
    data = Data()

    check_for_addition_start=256
    check_for_addition_control=256

    for k in range(1,data.ROUNDS):
        MESSAGE=b'\x00'*aes.NULLBYTES
        MESSAGE=str(base64.b64encode(MESSAGE))[2:-1]
        ENCRYPTED_MESSAGE = requests.post("http://" + data.IP_ADRESS + "/api/EcbDecryption/" + data.ACCNAME + "/" + str(k) + "/noentropy",headers={'Content-Type': 'application/json','charset':'utf-8'}, data=json.dumps(MESSAGE))
        MESSAGE=b'\x01'*aes.NULLBYTES
        MESSAGE=str(base64.b64encode(MESSAGE))[2:-1]
        ENCRYPTED_MESSAGE_1 = requests.post("http://" + data.IP_ADRESS + "/api/EcbDecryption/" + data.ACCNAME + "/" + str(k) + "/noentropy",headers={'Content-Type': 'application/json','charset':'utf-8'}, data=json.dumps(MESSAGE))
        ENCRYPTED_MESSAGE_bytes=base64.b64decode(ENCRYPTED_MESSAGE.text)
        ENCRYPTED_MESSAGE_1_bytes=base64.b64decode(ENCRYPTED_MESSAGE_1.text)
        if aes.ModeIS(ENCRYPTED_MESSAGE_bytes) == aes.ECB:
            check = requests.get("http://" + data.IP_ADRESS + "/api/EcbDecryption/" + data.ACCNAME + "/" + str(k) + "/verify",headers={'Content-Type': 'application/json', 'charset': 'utf-8'})
            for i in range(len(ENCRYPTED_MESSAGE_bytes)):
                if ENCRYPTED_MESSAGE_bytes[i]!=ENCRYPTED_MESSAGE_1_bytes[i]:
                    RANDPADLENGTH=i
                    break
            for i in range(17):
                MESSAGE = b'\x00'*aes.N
                MESSAGE = str(base64.b64encode(MESSAGE))[2:-1]
                ENCRYPTED_MESSAGE = requests.post("http://" + data.IP_ADRESS + "/api/EcbDecryption/" + data.ACCNAME + "/" + str(k) + "/noentropy",headers={'Content-Type': 'application/json', 'charset': 'utf-8'},data=json.dumps(MESSAGE))
                MESSAGE = b'\x00'*(aes.N+i)
                MESSAGE = str(base64.b64encode(MESSAGE))[2:-1]
                ENCRYPTED_MESSAGE_1 = requests.post("http://" + data.IP_ADRESS + "/api/EcbDecryption/" + data.ACCNAME + "/" + str(k) + "/noentropy",headers={'Content-Type': 'application/json', 'charset': 'utf-8'},data=json.dumps(MESSAGE))
                TMP_1 = base64.b64decode(ENCRYPTED_MESSAGE_1.text)[RANDPADLENGTH+aes.N]
                TMP_2 = base64.b64decode(ENCRYPTED_MESSAGE_1.text)[RANDPADLENGTH+aes.N+1]
                if check_for_addition_start!=TMP_1:
                    check_for_addition_start=TMP_1
                    check_for_addition_control=TMP_2
                else:
                    if check_for_addition_control!=TMP_2:
                        check_for_addition_control=TMP_2
                    else:
                        addition = i-1
                        break

            RESDATALEN=len(ENCRYPTED_MESSAGE_bytes)-(RANDPADLENGTH+aes.NULLBYTES)
            RESDATA=b''
            MESSAGE_1 = b'\x00' * (2*aes.N + RESDATALEN+addition)
            for i in range(RESDATALEN):
                MESSAGE = str(base64.b64encode(MESSAGE_1[:-i-1]))[2:-1]
                ENCRYPTED_MESSAGE = requests.post("http://" + data.IP_ADRESS + "/api/EcbDecryption/" + data.ACCNAME + "/" + str(k) + "/noentropy",headers={'Content-Type': 'application/json', 'charset': 'utf-8'},data=json.dumps(MESSAGE))
                ENCRYPTED_MESSAGE_bytes = base64.b64decode(ENCRYPTED_MESSAGE.text)
                MESSAGE_1 = MESSAGE_1[1:len(MESSAGE_1)] + bytes([0])
                for j in range(0,256):
                    MESSAGE_1=MESSAGE_1[0:len(MESSAGE_1)-1]+bytes([j])
                    MESSAGE = str(base64.b64encode(MESSAGE_1))[2:-1]
                    ENCRYPTED_MESSAGE_1 = requests.post("http://" + data.IP_ADRESS + "/api/EcbDecryption/" + data.ACCNAME + "/" + str(k) + "/noentropy",headers={'Content-Type': 'application/json', 'charset': 'utf-8'},data=json.dumps(MESSAGE))
                    ENCRYPTED_MESSAGE_1_bytes = base64.b64decode(ENCRYPTED_MESSAGE_1.text)
                    if ENCRYPTED_MESSAGE_bytes[:RANDPADLENGTH+len(MESSAGE_1)+aes.N-addition]==ENCRYPTED_MESSAGE_1_bytes[:RANDPADLENGTH+len(MESSAGE_1)+aes.N-addition]:
                        RESDATA+=bytes([j])
                        break
                if len(RESDATA)%10==0:
                    print('found data is', RESDATA,addition)
                    RESDATA=base64.b64decode(check.text)
                    print('RESDATA is', RESDATA)
                    break
        else:
            print('There is not ECB.')
            

if __name__ == '__main__':
    main()