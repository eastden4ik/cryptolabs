from Crypto.Cipher import AES
import os
from Crypto.Util import Counter
from Crypto.Util.number import bytes_to_long



class AES_SV(object):
    
    def __init__(self):
        self.N=16
        self.NULLBYTES=2048
        self.IV=os.urandom(self.N)
        self.ECB = 'ECB'
        self.CBC = 'CBC'
        self.OFB = 'OFB'
        self.CFB = 'CFB'
        self.CTR = 'CTR'

    def Padding(self, data):
        bytes_to_add = ['\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07', '\x08', '\x09', '\x10', '\x11', '\x12',
                        '\x13', '\x14', '\x15','\x00']
        k=self.N-len(data)%self.N
        data = data + k*bytes(bytes_to_add[k], 'utf-8')
        return data

    def AesBlockEncrypt(self,key,data):
        key = bytes(key, 'utf-8')
        data = bytes(data, 'utf-8')
        if len(data)>self.N:
            return print("The length of data is more, then length of block")
        else:
            data=self.Padding(data)
        cipher = AES.new(key, AES.MODE_ECB)
        encr=cipher.encrypt(data)
        return encr
    
    def AesBlockDecrypt(self,key,data):
        data = bytes.fromhex(data)
        cipher = AES.new(key, AES.MODE_ECB)
        decr=cipher.decrypt(data)
        return decr
        
    def AesEncrypt(self, key, data, mode, iv = ''):
        ans = b''
        finalblock = data[lenofblock * (len(data) // lenofblock):]
        data = data[:lenofblock * (len(data)) // lenofblock]
        blocks = [data[i:i + lenofblock] for i in range(0, len(data) - lenofblock + 1, lenofblock)]
        if mode == self.ECB:
            for block in blocks:
                ans = ans + AesBlockEncrypt(key, block)
        elif mode == self.CBC:
            if iv == '':
                iv = os.urandom(lenofblock)
            blocks[0] = xor(blocks[0], iv)
            c = AesBlockEncrypt(key, blocks[0])
            ans = iv + c
            for i in range(1, len(blocks)):
                c = AesBlockEncrypt(key, xor(blocks[i], c))
                ans = ans + c
                
        elif mode == self.CFB:
            if iv == '':
                iv = os.urandom(lenofblock)
            ans = ans + iv
            c = AesBlockEncrypt(key, iv)
            for i in range(0, len(blocks)):
                c = xor(blocks[i], c)
                ans = ans + c
                c = AesBlockEncrypt(key, c)
            
        elif mode == self.OFB:
            if iv == '':
                iv = os.urandom(lenofblock)
            ans = ans + iv
            c = AesBlockEncrypt(key, iv)
            for i in range(0, len(blocks)):
                ans = ans + xor(blocks[i], c)
                c = AesBlockEncrypt(key, c)
            
        elif mode == self.CTR:
            if iv == '':
                iv = os.urandom(lenofblock)
            ans = ans + iv
            for block in blocks:
                ans = ans + AesBlockEncrypt(key, xor(iv, block))
                iv = (int(iv.hex(), 16) + 1).to_bytes(16, 'big')
        
        return ans
    
    def AesDecrypt(self, key, data, mode):
        ans = b''
        blocks = [data[i:i + 16] for i in range(0, len(data) - 15, 16)]
        
        if mode == self.ECB:
            for block in blocks:
                ans = ans + AesBlockDecrypt(key, block)
            
        elif mode == self.CBC:
            for i in range(1, len(blocks)):
                c = (int(AesBlockDecrypt(key, blocks[i]).hex(), 16) ^ int(blocks[i - 1].hex(), 16)).to_bytes(16, 'big')
                ans = ans + c
                
        elif mode == self.CFB:
            for i in range(1, len(blocks)):
                ans = ans + (int(blocks[i].hex(), 16) ^ int(AesBlockEncrypt(key, blocks[i - 1]).hex(), 16)).to_bytes(16, 'big')
            
        elif mode == self.OFB:
            iv = blocks[0]
            c = AesBlockEncrypt(key, iv)
            for i in range(1, len(blocks)):
                ans = ans + (int(blocks[i].hex(), 16) ^ int(c.hex(), 16)).to_bytes(16, 'big')
                c = AesBlockEncrypt(key, c)
            
        elif mode == self.CTR:
            iv = blocks[0]
            for i in range(1, len(blocks)):
                ans = ans + (int(AesBlockDecrypt(key, blocks[i]).hex(), 16) ^ int(iv.hex(), 16)).to_bytes(16, 'big')
                iv = (int(iv.hex(), 16) + 1).to_bytes(16, 'big')
        return ans

    def AesEncryptREGULAR(self,key,data,mode,iv):
        key = bytes.fromhex(key)
        #data = bytes(data, 'utf-8')
        if mode==self.CBC:
            cipher = AES.new(key,AES.MODE_CBC,self.IV)
        elif mode==self.CFB:
            cipher = AES.new(key, AES.MODE_CFB, self.IV)
        elif mode==self.OFB:
            cipher = AES.new(key, AES.MODE_OFB, self.IV)
        elif mode==self.CTR:
            iv = os.urandom(int(self.N))
            counter = Counter.new(self.N*8, initial_value=bytes_to_long(self.IV))
            cipher = AES.new(key, AES.MODE_CTR, counter=counter)
        else:
            print("There is not any knowable cipher")
            return "error"

        #print('Clear_data', data)
        Encr_data=(self.IV+cipher.encrypt(data)).hex()

        return Encr_data
    
    def AesDecryptREGULAR(self,key,data,mode):
        key=bytes.fromhex(key)
        message_c=bytes.fromhex(data)[self.N:]
        self.IV=bytes.fromhex(data)[:self.N]

        if mode == self.CBC:
            cipher = AES.new(key, AES.MODE_CBC, self.IV)
        elif mode == self.CFB:
            cipher = AES.new(key, AES.MODE_CFB, self.IV)
        elif mode == self.OFB:
            cipher = AES.new(key, AES.MODE_OFB, self.IV)
        elif mode == self.CTR:
            counter = Counter.new(self.N*8, initial_value=bytes_to_long(self.IV))
            cipher = AES.new(key, AES.MODE_CTR, counter=counter)
        else:
            return "Error. There is not any knowable cipher"

        return (cipher.decrypt(message_c))
            
    def ModeIS(self,text):
        text=str(text)[2:-1]
        for i in range(len(text)):
            if text.count(text[int(len(text)/2):int(len(text)/2)+self.N])>1:
                return self.ECB
            else:
                return self.CBC
