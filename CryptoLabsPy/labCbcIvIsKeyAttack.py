import requests

from AES_SV import AES_SV as AES
from DATA import Data


def XOR(a,b):
    p=b''
    a=bytes.fromhex(a)
    b=bytes.fromhex(b)
    for i in range(len(a)-len(b),len(a),1):
        d = a[i] ^ b[i-(len(a)-len(b))]
        p+=bytes([d])
    p=a[:len(a)-len(b)]+p
    return p

def main():

    aes = AES()
    data = Data()
    
    r_user_encr = requests.get("http://"+data.IP_ADRESS+"/api/IvIsKey/" + data.ACCNAME + "/"+str(1)+"/User/encryptedToken/hex", headers={'Content-Type': 'application/json'})
    to_send=bytes.fromhex(r_user_encr.text[:32])+b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'+\
            bytes.fromhex(r_user_encr.text[:32])+b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'

    for i in range(20):
        requestUserClear = requests.get("http://"+data.IP_ADRESS+"/api/IvIsKey/" + data.ACCNAME + "/"+str(i)+"/User/Token/", headers={'Content-Type': 'application/json'})
        requestAdminClear = requests.get("http://"+data.IP_ADRESS+"/api/IvIsKey/" + data.ACCNAME + "/"+str(i)+"/Admin/Token/", headers={'Content-Type': 'application/json'})
        requestUserAuth = requests.get("http://"+data.IP_ADRESS+"/api/IvIsKey/" + data.ACCNAME + "/"+str(i)+"/authenticate/user/encryptedtoken="+to_send.hex(),headers={'Content-Type': 'application/json'})
        key_row=requestUserAuth.text[148:-1]
        key=XOR(key_row[:32],key_row[64:96])

        cipher = aes.AesEncrypt(key,requestUserClear.text,aes.CBC,key)
        requestUserAuth = requests.get("http://"+data.IP_ADRESS+"/api/IvIsKey/" + data.ACCNAME + "/"+str(i)+"/authenticate/user/encryptedtoken="+cipher.hex(),headers={'Content-Type': 'application/json'})
        cipher = aes.AesEncrypt(key,requestAdminClear.text,aes.CBC,key)
        requestAdminAuth = requests.get("http://"+data.IP_ADRESS+"/api/IvIsKey/" + data.ACCNAME + "/"+str(i)+"/authenticate/admin/encryptedtoken="+cipher.hex(),headers={'Content-Type': 'application/json'})

        print('Test  - ',i+1,requestAdminAuth.text)
        
if __name__ == '__main__':
    main()