import json
import requests
import base64

from DATA import Data

        
def main():
    
    data = Data()
    
    i=1
    message=b'8655\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
    print()
    pin_encr = requests.get("http://"+data.IP_ADRESS+"/api/IvIsTime/" + data.ACCNAME + "/"+str(i)+"/encryptedpin", headers={'Content-Type': 'application/json'})
    message = str(base64.b64encode(message))[2:-1]
    encr_text = requests.post("http://"+data.IP_ADRESS+"/api/IvIsTime/" + data.ACCNAME + "/"+str(i)+"/noentropy", headers={'Content-Type': 'application/json'}, data=json.dumps(message))

    for i in range(1,20,1):
        print('Task number',i)
        pin_clear = requests.get("http://" + data.IP_ADRESS + "/api/IvIsTime/" + data.ACCNAME + "/" + str(i) + "/validate",
                                headers={'Content-Type': 'application/json'})
        print('Validation pin is', base64.b64decode(pin_clear.text))
        for j in range(0,10**2,1):

            message = bytes(str(j), 'utf-8')
            message = str(base64.b64encode(message))[2:-1]
            if j%100==0:
                pin_encr = requests.get("http://" + data.IP_ADRESS + "/api/IvIsTime/" + data.ACCNAME + "/" + str(i) + "/encryptedpin",
                                        headers={'Content-Type': 'application/json'})
            encr_text = requests.post("http://" + data.IP_ADRESS + "/api/IvIsTime/" + data.ACCNAME + "/" + str(i) + "/noentropy",
                                    headers={'Content-Type': 'application/json'}, data=json.dumps(message))
            if pin_encr.text==encr_text.text:
                print('Pin code is',j,'\n')
                break
            pass


if __name__ == '__main__':
    main()