
import os
from AES_SV import AES_SV as AES # In class AES_SV methods for Task1 and Task2



def main():
    aes = AES()
    key='Sixteen-byte-key'
    data='Sixteen-byte-msg'
    key = bytes(key, 'utf-8').hex()
# Task1
    checker=aes.AesBlockEncrypt(key,data)
    checker_decr=aes.AesBlockDecrypt(key,checker)
    print('Text before encryption is:',data)
    print('Encrypted data is:', checker)
    print('Decrypted data is:', checker_decr)
# Task2
    data_task_2 = b'Sixteen-byte-msgSixteen-byte-msg01234567'
    key_task_2 = '140b41b22a29beb4061bda66b6747e14'
    if aes.AesEncrypt(key_task_2,data_task_2,aes.ECB).hex() == aes.AesEncryptREGULAR(key_task_2,data_task_2,aes.ECB):
        print("Good!!! Encryption methods equal.")
    if aes.AesEncrypt(key_task_2,data_task_2,aes.CBC,os.urandom(aes.N)).hex() == aes.AesEncryptREGULAR(key_task_2,data_task_2,aes.CBC,os.urandom(aes.N)):
        print("Good!!! Encryption methods equal.")
    if aes.AesEncrypt(key_task_2,data_task_2,aes.CFB,os.urandom(aes.N)).hex() == aes.AesEncryptREGULAR(key_task_2,data_task_2,aes.CFB,os.urandom(aes.N)):
        print("Good!!! Encryption methods equal.")
    if aes.AesEncrypt(key_task_2,data_task_2,aes.OFB,os.urandom(aes.N)).hex() == aes.AesEncryptREGULAR(key_task_2,data_task_2,aes.OFB,os.urandom(aes.N)):
        print("Good!!! Encryption methods equal.")
    if aes.AesEncrypt(key_task_2,data_task_2,aes.CTR,os.urandom(aes.N)).hex() == aes.AesEncryptREGULAR(key_task_2,data_task_2,aes.CTR,os.urandom(aes.N)):
        print("Good!!! Encryption methods equal.")
    
    if aes.AesDecrypt(key_task_2,aes.AesEncrypt(key_task_2,data_task_2,aes.ECB),aes.ECB) == aes.AesEncryptREGULAR(key_task_2,aes.AesEncryptREGULAR(key_task_2,data_task_2,aes.ECB),aes.ECB):
        print("Good!!! Decryption methods equal.")
    if aes.AesDecrypt(key_task_2,aes.AesEncrypt(key_task_2,data_task_2,aes.CBC,os.urandom(aes.N)),aes.CBC) == aes.AesDecryptREGULAR(key_task_2,aes.AesEncryptREGULAR(key_task_2,data_task_2,aes.CBC,os.urandom(aes.N),aes.CBC):
        print("Good!!! Decryption methods equal.")
    if aes.AesDecrypt(key_task_2,aes.AesEncrypt(key_task_2,data_task_2,aes.CFB,os.urandom(aes.N)),aes.CFB) == aes.AesDecryptREGULAR(key_task_2,aes.AesEncryptREGULAR(key_task_2,data_task_2,aes.CFB,os.urandom(aes.N),aes.CFB):
        print("Good!!! Decryption methods equal.")
    if aes.AesDecrypt(key_task_2,aes.AesEncrypt(key_task_2,data_task_2,aes.OFB,os.urandom(aes.N)),aes.OFB) == aes.AesDecryptREGULAR(key_task_2,aes.AesEncryptREGULAR(key_task_2,data_task_2,aes.OFB,os.urandom(aes.N),aes.OFB):
        print("Good!!! Decryption methods equal.")
    if aes.AesDecrypt(key_task_2,aes.AesEncrypt(key_task_2,data_task_2,aes.CTR,os.urandom(aes.N)),aes.CTR) == aes.AesDecryptREGULAR(key_task_2,aes.AesEncryptREGULAR(key_task_2,data_task_2,aes.CTR,os.urandom(aes.N),aes.CTR):
        print("Good!!! Decryption methods equal.")
# Task3
    key='140b41b22a29beb4061bda66b6747e14'
    data_0='4ca00ff4c898d61e1edbf1800618fb2828a226d160dad07883d04e008a7897ee2e4b7465d5290d0c0e6c6822236e1daafb94ffe0c5da05d9476be028ad7c1d81'
    data_1='5b68629feb8606f9a6667670b75b38a5b4832d0f26e1ab7da33249de7d4afc48e713ac646ace36e872ad5fb8a512428a6e21364b0c374df45503473c5242a253'
    key_1='36f18357be4dbd77f050515c73fcf9f2'  
    data_2='69dda8455c7dd4254bf353b773304eec0ec7702330098ce7f7520d1cbbb20fc388d1b0adb5054dbd7370849dbf0b88d393f252e764f1f5f7ad97ef79d59ce29f5f51eeca32eabedd9afa9329'    
    data_3='770b80259ec33beb2561358a9f2dc617e46218c0a53cbeca695ae45faa8952aa0e311bde9d4e01726d3184c34451'
    print('Text 1 is:', aes.AesDecryptREGULAR(key,data_0,aes.CBC))
    print('Text 2 is:', aes.AesDecryptREGULAR(key,data_1,aes.CBC))
    print('Text 3 is:', aes.AesDecryptREGULAR(key_1,data_2,aes.CTR))
    print('Text 4 is:', aes.AesDecryptREGULAR(key_1,data_3,aes.CTR))
    
# Task4
    data=b'Sixteen-byte-msgSixteen-byte-msg01234567'
    Encr_1=aes.AesEncryptREGULAR(key,aes.Padding(data),aes.CBC,os.urandom(aes.N))
    Decr_1=aes.AesDecryptREGULAR(key,Encr_1,aes.CBC)

    print(aes.CBC)
    print('Clear text is:', data)
    print('Encr_text is:', Encr_1)
    print('Decr_text is:',Decr_1)

    print(aes.CFB)
    Encr_2=aes.AesEncryptREGULAR(key,aes.Padding(data),aes.CFB,os.urandom(aes.N))
    Decr_2=aes.AesDecryptREGULAR(key,Encr_2,aes.CFB)
    print('Encr_text is:', Encr_2)
    print('Decr_text is:',Decr_2)

    print(aes.OFB)
    Encr_3=aes.AesEncryptREGULAR(key,aes.Padding(data),aes.OFB,os.urandom(aes.N))
    Decr_3=aes.AesDecryptREGULAR(key,Encr_3,aes.OFB)
    print('Encr_text is:', Encr_3)
    print('Decr_text is:',Decr_3)

    print(aes.CTR)
    Encr_4=aes.AesEncryptREGULAR(key,aes.Padding(data),aes.CTR,os.urandom(aes.N))
    Decr_4=aes.AesDecryptREGULAR(key,Encr_4,aes.CTR)
    print('Encr_text is:', Encr_4)
    print('Decr_text is:',Decr_4)



if __name__ == '__main__':
    main()


