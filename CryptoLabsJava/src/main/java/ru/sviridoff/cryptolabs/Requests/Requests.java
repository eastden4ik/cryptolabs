package ru.sviridoff.cryptolabs.Requests;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

public class Requests {

    private String URI;
    private String Result;

    private CloseableHttpClient httpClient = HttpClients.createDefault();

    public String GET(String url) throws Exception {
        this.URI = url;
        HttpGet request = new HttpGet(this.URI);
        request.addHeader("Content-Type", "application/json");
        try (CloseableHttpResponse response = httpClient.execute(request)){
            System.out.println("STATUS: " + response.getStatusLine().toString());
            HttpEntity entity = response.getEntity();
            Header headers = entity.getContentType();
            System.out.println("HEADERS: " + headers.toString());
            this.Result = EntityUtils.toString(entity);
        }
        return this.Result;
    }

    public String POST(String url, String RequestName, String RequestValue) throws Exception {
        this.URI = url;
        HttpPost request = new HttpPost(this.URI);
        List<NameValuePair> urlParams = new ArrayList<>();
        urlParams.add(new BasicNameValuePair(RequestName, RequestValue));
        request.setEntity(new UrlEncodedFormEntity(urlParams));
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {
            this.Result = EntityUtils.toString(response.getEntity());
        }
        return  this.URI;
    }

}
