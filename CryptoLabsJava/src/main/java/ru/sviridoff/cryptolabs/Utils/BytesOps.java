package ru.sviridoff.cryptolabs.Utils;

import java.util.ArrayList;
import java.util.List;

public class BytesOps {

    private List<Byte> list1;
    private List<Byte> list2;
    private int SIZE_IV;

    public List<Byte> Xor(List<Byte> list1, List<Byte> list2) throws Exception {
        List<Byte> result = new ArrayList<>();

        this.list1 = list1;
        this.list2 = list2;

        if ((this.list1.size() == this.list2.size()) && (this.list1.size() != 0) && (this.list2.size() != 0)) {
            for (int i = 0; i < this.list1.size(); i++) {
                result.add(Byte.parseByte(Integer.toString(this.list1.get(i) ^ this.list2.get(i))));
            }
        } else {
            throw new Exception("Some troubles with arrays. Check it. Array1: " + this.list1 + ", Array2: " + this.list2);
        }

        return result;
    }

    public List<Byte> GetIV(List<Byte> list1, int SIZE) throws Exception {
        List<Byte> result = new ArrayList<>();
        this.list1 = list1;
        this.SIZE_IV = SIZE;

        if (this.list1.size() != 0) {
            if (this.SIZE_IV != 0) {

                for (int i = 0; i < this.SIZE_IV; i++) {
                    result.add(this.list1.get(i));
                }
            } else {
                throw new Exception("IV Can't be null.");
            }
        } else {
            throw new Exception("List of bytes Can't be null.");
        }

        return result;
    }

    public List<Byte> GetPartList(List<Byte> list1, int from, int to) throws Exception {
        List<Byte> result = new ArrayList<>();
        this.list1 = list1;

        if (this.list1.size() != 0) {
            for (int i = from; i < to + 1; i++) {
                result.add(this.list1.get(i));
            }
        } else {
            throw new Exception("List of bytes Can't be null.");
        }

        return result;
    }

}
