package ru.sviridoff.cryptolabs.Labs;

import ru.sviridoff.cryptolabs.Requests.Requests;
import ru.sviridoff.cryptolabs.Utils.BytesOps;
import ru.sviridoff.cryptolabs.Utils.StashClass;

import java.io.IOException;

public class Lab1Steps {

    private BytesOps bytes;
    private Requests requests;

    // Check the service
    public void Step1() throws Exception {
        String URI = "http://" + StashClass.ip_address + "/api/Sha1Mac";
        String result = requests.GET(URI);
        if (result.equals("operating")) {
            System.out.println(true);
        }
    }

}


/*
 * TODO:
 *  Step 1. Get request {ip}/api/Sha1Mac
 *  Step 2. Get request to receive encrypted message {ip}/api/Sha1Mac/{UserId}/{ChallengeId}/mac
 *  Step 3. Receive text ";admin=true" and its mac
 *  Step 4. Post request {ip}/api/Sha1Mac/{UserId}/{ChallengeId}/{mac}/verify - must receive message "Welcome to secretNet!"
 * */