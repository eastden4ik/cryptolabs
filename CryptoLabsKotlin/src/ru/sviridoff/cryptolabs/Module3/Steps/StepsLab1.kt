package ru.sviridoff.cryptolabs.Module3.Steps

import ru.sviridoff.cryptolabs.Module3.Requests.GET
import ru.sviridoff.cryptolabs.Module3.Requests.POST
import ru.sviridoff.cryptolabs.Module3.Utils.Stash
import javax.crypto.Cipher

class StepsLab1() {

    // Проверка работы сервиса.
    public fun step1() {
        val res = GET(Stash().IP + "/api/sha1mac/")
        if (res.equals("operating")) {
            println("Service is working.")
        }
    }

    // Получение зашифрованного МАКа
    public fun step2() {
        val res = GET(Stash().IP + "/api/sha1mac/" + Stash().UserId + "/" + Stash().ChallengeId + "/mac")
        Stash().mac = res
        println(Stash().mac)
    }

    //
    public fun step3() {
        val res = POST(Stash().IP + "/api/sha1mac/" + Stash().UserId + "/" + Stash().ChallengeId + "/" + Stash().mac + "/verify", ";admin=true")
        println(res)
    }



}