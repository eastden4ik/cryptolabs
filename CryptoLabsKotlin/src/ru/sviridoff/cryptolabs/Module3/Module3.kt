package ru.sviridoff.cryptolabs.Module3

import ru.sviridoff.cryptolabs.Module3.Utils.Stash as Stash
import ru.sviridoff.cryptolabs.Module3.Requests.GET
import ru.sviridoff.cryptolabs.Module3.Steps.StepsLab1

fun main(){

    val steps = StepsLab1()

    steps.step1()
    steps.step2()
    steps.step3()

}